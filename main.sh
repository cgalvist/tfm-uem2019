#!/bin/bash
# -*- ENCODING: UTF-8 -*-

# This software is licensed under the terms of the GNU General Public
# License version 2, as published by the Free Software Foundation, and
# may be copied, distributed, and modified under those terms.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# Please maintain this if you use this script or any part of it
#
# Copyright © 2019, César Galvis "cgalvist"
#

#########################

# Script para integrar el proceso de configuración e instalación de herramientas requeridas para analizar la memoria en Android

# Con este script se podrá:

# - Seleccionar solo una vez la arquitectura para todas las opciones disponibles (compilado del kernel, LiME y Volatility)
# - Compilar kernel personalizado con las opciones requeridas para instalar LiME
# - Compilar módulo de kernel de LiME
# - Instalar módulo de LiME en un dispositivo Android o máquina virtual
# - Iniciar una máquina virtual con un kernel personalizado en AVD

#################
### VARIABLES ###
#################

# importar variables personalizadas del usuario
source .user_config.sh

# importar variables definidas de rutas y programas
source Scripts/variables.sh

# Código de colores del script
Black='\e[0;30m'        # Black
Red='\e[0;31m'          # Red
Green='\e[0;32m'        # Green
Yellow='\e[0;33m'       # Yellow
Blue='\e[0;34m'         # Blue
Purple='\e[0;35m'       # Purple
Cyan='\e[0;36m'         # Cyan
White='\e[0;37m'        # White
nocol='\033[0m'         # Default

##################
### SENTENCIAS ###
##################

# 
# Ayuda
# 
function help(){
    echo -e "Uso: main.sh [OPCIONES] -- Programa para facilitar la instalación de programas y entornos \npara el análisis forense de RAM. \n"
    echo -e "  -h, --help \t\t\t\t Muestra esta ayuda."
    echo -e "  -config, --configure \t\t\t Permite al usuario modificar algunos parámetros."
    echo -e "  -lc,--list-config \t\t\t Muestra la configuración del usuario"
    echo -e "  -ddep, --download-dependences \t Descarga dependencias requeridas del script."
    echo -e "  -dkernel, --download-kernel \t\t Descarga el kernel y Toolchain según la \n\t\t\t\t\t configuración del usuario"
    echo -e "  -copykernelc, --copy-kernel-config \t Copia el archivo de configuración ubicado en \n\t\t\t\t\t \"scripts/kernel_custom.config\" a la carpeta \n\t\t\t\t\t del kernel."
    echo -e "  -ckernel, --compile-kernel \t\t Prepara los módulos del kernel y luego lo compila."
    echo -e "  -semu, --start-emulator \t\t Inicia el emulador AVD con el nombre \n\t\t\t\t\t especificado por el usuario y con el kernel \n\t\t\t\t\t compilado."
    echo -e "  -ilime, --install-lime \t\t Compila el módulo de kernel (LKM) de LiMe y lo \n\t\t\t\t\t instala en el emulador o dispositivo conectado \n\t\t\t\t\t por adb."
    echo -e "  -memextract, --extract-memory \t Extrae la memoria del dispositivo con LiME en la \n\t\t\t\t\t carpeta de instalación de Volatility con el \n \t\t\t\t\t nombre \"$LIME_DUMP_NAME\"."
    echo -e "  -ivol, --volatility-install \t\t Instala Volatility en el sistema."
    echo -e "  -genprofile, --generate-profile \t Genera el perfil de volatility con el kernel \n\t\t\t\t\t compilado."
}

# 
# Función para configurar los parámetros del programa
# 
function config_environment(){

    echo
    echo -e "$Yellow*************************************"
    echo    "     Configuración de variables     "
    echo -e "*************************************$nocol"
    echo
    
    echo "Seleccione una arquitectura:"
    echo "[0] x86_64"
    echo "[1] x86"
    echo "[2] arm64"
    echo "[3] arm"
    read -p "[x86_64]: " temp_arch
    temp_arch=${temp_arch:-0}
    
    # detectar si la entrada es un entero y si está entre el rango válido
    if ! [[ $temp_arch =~ ^-?[0-9]+$ && $temp_arch -gt -1 && $temp_arch -lt 5 ]]; 
    then
        echo "Opción \"$temp_arch\" inválida".
        exit
    fi
    
    user_config=""
    
    case $temp_arch in
    0)
        user_config+="export ARCH=x86_64;export SUBARCH=x86_64;export POSTFIX=-linux-android-;";
        shift
        ;;
    1)
        user_config+="export ARCH=x86;export SUBARCH=i686;export POSTFIX=-linux-android-;"
        shift
        ;;
    2)
        user_config+="export ARCH=arm64;export SUBARCH=aarch64;export POSTFIX=-linux-android-;"
        shift
        ;;
    3)
        user_config+="export ARCH=arm;export SUBARCH=arm;export POSTFIX=-linux-androideabi-;"
        shift
        ;;
    esac
    
    echo
    echo "Seleccione una versión del kernel:"
    echo "[0] 3.10.0"
    echo "[1] 3.18"
    read -p "[3.10.0]: " temp_kver
    temp_kver=${temp_kver:-0}
    
    # detectar si la entrada es un entero y si está entre el rango válido
    if ! [[ $temp_kver =~ ^-?[0-9]+$ && $temp_kver -gt -1 && $temp_kver -lt 2 ]]; 
    then
        echo "Opción \"$temp_kver\" inválida".
        exit
    fi
    
    case $temp_kver in
    0)
        user_config+="export KVER=3.10.0;";
        shift
        ;;
    1)
        user_config+="export KVER=3.18;"
        shift
        ;;
    esac
    
    echo
    echo "Escriba el nombre de la máquina virtual:"
    read -p "[API_22_x86_64]: " temp_machine_name
    temp_machine_name=${temp_machine_name:-"API_22_x86_64"}
    
    user_config+="export AVD_MACHINE_NAME=$temp_machine_name;"
    
    # guardar configuración
    echo "$user_config" > .user_config.sh
    source .user_config.sh
    source Scripts/variables.sh
    
    # listar variables
    show_environment
}

# 
# Función para mostrar entorno del programa
# 
function show_environment(){
    echo
    
    echo -e "$Purple**************************"
    echo    "         Ambiente        "
    echo -e "**************************$nocol"
    
    echo
    echo -e "* Arquitectura: \t\t\t$ARCH"
    echo -e "* Versión del kernel: \t\t\t$KVER"
    echo -e "* Nombre del kernel: \t\t\t$KERNEL_OUT"
    echo -e "* Ruta del emulador: \t\t\t$EMU_SRC"
    echo -e "* Nombre de la máquina virtual: \t$AVD_MACHINE_NAME"
    echo
}

# 
# Función para descargar dependencias
# 
function download_dependences(){

    # listar variables
    show_environment
    
    echo -e "$Green******************************************"
    echo    "         Descargar dependencias         "
    echo -e "******************************************$nocol"
    
    echo -e "$Yellow Descargando LiME: $nocol"
    cd $MAIN_PATH
    git clone https://github.com/504ensicsLabs/LiME LiME
    
    echo -e "$Yellow Descargando Volatility: $nocol"
    cd $MAIN_PATH
    git clone https://github.com/volatilityfoundation/volatility Volatility
}

# 
# Función para descargar kernel y toolchain
# 
function download_kernel(){

    # listar variables
    show_environment
    
    echo -e "$Green*************************************"
    echo    "         Descargando kernel         "
    echo -e "*************************************$nocol"
    
    echo
    echo -e "$Yellow Descargando kernel: $nocol"
    echo
    
    temp_kernel_download=""        
    cd $MAIN_PATH
    
    # crear directorio si no existe
    mkdir -p $KSRC_PATH
    
    case $KVER in
    3.10.0)
        temp_kernel_download="git clone -b android-goldfish-3.10-n-dev https://android.googlesource.com/kernel/goldfish.git/ $KSRC_PATH";
        shift
        ;;
    3.18)
        temp_kernel_download="git clone -b android-goldfish-3.18 https://android.googlesource.com/kernel/goldfish.git/ $KSRC_PATH";
        shift
        ;;
    *)
        echo "Versión de kernel inválida: $KVER"
        exit
    esac
    
    $temp_kernel_download
    
    echo
    echo -e "$Yellow Descargando Toolchain: $nocol"
    echo
    
    temp_toolchain_download=""        
    cd $MAIN_PATH
    
    # crear directorio si no existe
    mkdir -p $TC_PATH
    
    case $ARCH in
    x86_64)
        temp_toolchain_download="git clone -b ndk-r19c https://android.googlesource.com/platform/prebuilts/gcc/linux-x86/x86/x86_64-linux-android-4.9/ $TC_PATH";
        shift
        ;;
    x86)
        temp_toolchain_download="git clone -b master https://android.googlesource.com/platform/prebuilts/gcc/linux-x86/x86/i686-linux-android-4.7 $TC_PATH";
        shift
        ;;
    ARM64)
        temp_toolchain_download="git clone -b ndk-r19c https://android.googlesource.com/platform/prebuilts/gcc/linux-x86/aarch64/aarch64-linux-android-4.9 $TC_PATH";
        shift
        ;;
    ARM)
        temp_toolchain_download="git clone -b ndk-r19c https://android.googlesource.com/platform/prebuilts/gcc/linux-x86/arm/arm-linux-androideabi-4.9 $TC_PATH";
        shift
        ;;
    *)
        echo "Arquitectura inválida: $ARCH"
        exit
    esac
    
    $temp_toolchain_download
}

#
# Función pàra copiar la configuración del kernel para que sea compatible con LiME
# 
function copy_kernel_config(){

    # listar variables
    show_environment
    
    echo -e "$Yellow*****************************************"
    echo    "    Copia de configuración del kernel    "
    echo -e "*****************************************$nocol"

    echo
    echo -e "Es necesario generar un archivo de configuración del kernel para poder \ncompilarlo y también para compilar el módulo de LiME."
    echo -e "En el código existe un ejemplo en \"Example_configs/kernel_custom.config\", \nel cual ha sido probado exitosamente con una máquina virtual de$Green \nAndroid$nocol con$Green API 22$nocol, con$Green kernel 3.10.0$nocol y$Green arquitectura x86_64$nocol."
    
    echo
    echo -e "En caso de utilizar otra API o dispositivo puede que la configuración \ngenere errores en el kernel o con Volatility, por lo que si su \nconfiguración es distinta a la especificada$Red DEBE$nocol copiar su configuración \nen la carpeta$Red  \"Example_configs\"$nocol con el nombre$Red  \"kernel_custom.config\"$nocol."
    
    echo
    echo -e "Si puede obtener el archivo .config de otra fuente, recuerde configurar \nlas siguientes opciones:"
    
    echo
    echo -e "\tRequerimientos para permitir instalar módulos de kernel:"
    echo -e "\t\tCONFIG_MODULES=y"
    echo -e "\t\tCONFIG_MODULE_UNLOAD=y"
    echo -e "\t\tCONFIG_MODULE_FORCE_UNLOAD=y"
    echo -e "\tDesactivar SELinux al inicio:"
    echo -e "\t\tCONFIG_SECURITY_SELINUX_BOOTPARAM=y"
    echo -e "\t\tCONFIG_SECURITY_SELINUX_BOOTPARAM_VALUE=0"
    echo -e "\tDesactivar firma en los módulos de kernel:"
    echo -e "\t\t# CONFIG_MODULE_SIG is not set"
    echo -e "\tDesactivar que el kernel cambie de nombre automáticamente:"
    echo -e "\t\t# CONFIG_LOCALVERSION_AUTO is not set"
    
    echo
    echo -e "¿Desea copiar la configuración de la ruta \"Example_configs/kernel_custom.config\"?:"
    read -p "[y/N]: " temp_copy_config
    temp_copy_config=${temp_copy_config:-"n"}
    
    if [ $temp_copy_config == "y" ]; 
    then
        
        cp $MAIN_PATH/Example_configs/kernel_custom.config $KSRC_PATH/.config
        echo "Se ha copiado la configuración de ejemplo en la ruta $KSRC_PATH/.config".
        
    else
        echo "No se ha copiado la configuración de ejemplo."
    fi
}


#
# Función para compilar el kernel
# 
function kernel_compile(){

    # listar variables
    show_environment
    
    echo -e "$Green*********************************"
    echo    "         Compilar kernel         "
    echo -e "*********************************$nocol"

    echo
    echo -e "$Yellow Preparación de módulos del kernel: $nocol"
    echo
    
    cd $KSRC_PATH
    cp $MAIN_PATH/Example_configs/kernel_custom.config $KSRC_PATH/.config
    make ARCH=$ARCH CROSS_COMPILE=$CROSS_COMPILE modules_prepare
    
    echo
    echo -e "$Yellow Compilando el kernel: $nocol"
    echo
    
    cd $KSRC_PATH
    cp $MAIN_PATH/Scripts/kernel_build.sh $KSRC_PATH
    ./kernel_build.sh
}

# 
# Función para ejecutar el emulador de AVD con el kernel compilado 
# 

function emulator_start(){

    # listar variables
    show_environment
    
    echo -e "$Green*************************************"
    echo    "         Iniciando emulador         "
    echo -e "*************************************$nocol"
    
    $EMU_SRC/emulator @$AVD_MACHINE_NAME -kernel $KSRC_PATH/arch/$ARCH/boot/$KERNEL_OUT -writable-system -show-kernel -no-snapshot-load -ranchu
}

# 
# Función para compilar e instalar módulo de lime
# 
function lime_install(){
    
    echo -e "$Green*************************************"
    echo    "         Instalación de LiME         "
    echo -e "*************************************$nocol"
    
    echo -e "Antes de la instalación, asegúrese que el dispositivo o máquina virtual está en \nfuncionamiento, tiene permisos root con adb y tiene depuración usb habilitada."
    
    echo
    echo "¿Tiene el dispositivo listo, conectado y con permisos habilitados?:"
    read -p "[Y/n]: " android_device_ready
    android_device_ready=${android_device_ready:-"y"}
    
    if [ $android_device_ready == "y" ]; 
    then
        
        echo
        echo -e "$Yellow Compilando módulo de LiME: $nocol"
        echo
        
        cp $MAIN_PATH/Scripts/LiME_Makefile $LIME_SRC/Makefile
        cd $LIME_SRC
        make
        
        echo
        echo -e "$Yellow Enviando módulo en el dispositivo: $nocol"
        echo
        
        $ADB push lime-goldfish.ko /data/local/tmp/LiME/lime-goldfish.ko
        $ADB forward tcp:$LIME_HOST_PORT tcp:$LIME_DEVICE_PORT
        
        echo
        echo -e "$Yellow Ejecutando módulo en primer plano: $nocol"
        echo
        
        $ADB shell su -c insmod /data/local/tmp/LiME/lime-goldfish.ko "path=tcp:4444 format=lime"
        
        echo
        echo -e "$Yellow El módulo ha sido ingresado en el dispositivo. En caso de algún problema "
        echo -e "puede ver los logs de LiME en el sistema ejecutando la órden "
        echo -e "\"dmesg | grep lime\" dentro de una consola de adb $nocol"
        echo        
        
    else
        echo "No se ha procedido a la instalación del módulo de LiME"
    fi
}

# 
# Función para instalar volatility (solo una vez)
# 
function volatility_install(){
    
    echo -e "$Green*************************************"
    echo    "      Instalación de Volatility      "
    echo -e "*************************************$nocol"
    
    cd $VOL_SRC
    echo "sudo python setup.py install"
    sudo python setup.py install
}

# 
# Función para generar perfil de Volatility
# 
function volatility_build_profile(){
    
    echo -e "$Green******************************************"
    echo    "     Creación de perfil de Volatility     "
    echo -e "******************************************$nocol"
        
    echo
    echo -e "$Yellow Compilando perfil: $nocol"
    echo
    
    cd $VOL_SRC/tools/linux
    cp $MAIN_PATH/Scripts/Volatility_Makefile $VOL_SRC/tools/linux/Makefile
    make
        
    echo
    echo -e "$Yellow Empaquetando perfil: $nocol"
    echo
    
    cd $VOL_SRC/volatility/plugins/overlays/linux/
    zip Golfish-$KVER.zip $VOL_SRC/tools/linux/module.dwarf $KSRC_PATH/System.map
        
    echo
    echo -e "$Yellow El perfil ha sido guardado en la ruta \n\"$VOL_SRC/volatility/plugins/overlays/linux/\". \nVerifique que ha sido instalada correctamente ejecutando las siguientes órdenes:$nocol"
    echo
    
    echo -e "$Cyan cd $VOL_SRC$nocol"
    echo -e "$Cyan python vol.py --info | grep Linux$nocol"    
    echo
    
}

function lime_extract_mem(){
    
    echo -e "$Green****************************"
    echo    "     Extrayendo memoria     "
    echo -e "****************************$nocol"
    
    cd $VOL_SRC
    nc localhost $LIME_HOST_PORT > "$LIME_DUMP_NAME"
    
    echo
    echo -e "Se ha extraido la memoria en el archivo \"$LIME_DUMP_NAME\" en la ubicación \n\"$VOL_SRC\""
    echo

}

if ! [ -n "$1" ]; then
    help
    exit
fi

case "$1" in
-config|--configure)    
    config_environment    
    shift
    ;;
-ddep| --download-dependences)    
    download_dependences        
    shift
    ;;
-dkernel|--download-kernel)        
    download_kernel        
    shift
    ;;
-copykernelc|--copy-kernel-config)    
    copy_kernel_config        
    shift
    ;;
-ckernel|--compile-kernel)        
    kernel_compile        
    shift
    ;;
-semu|--start-emulator)    
    emulator_start        
    shift
    ;;
-ilime|--install-lime)    
    lime_install        
    shift
    ;;
-ivol|--install-volatility)
    volatility_install
    shift
    ;;
-genprofile|--generate-profile)
    volatility_build_profile
    shift
    ;;
-memextract|--extract-memory)
    lime_extract_mem
    shift
    ;;
# -c) # eliminar dependencias y kernel
#     # PENDIENTE
#     shift
#     ;;
-lc|--list-config) # listar variables    
    show_environment    
    shift
    ;;
-h|--help) # ayuda
    help
    shift
    ;;
*) help
esac

exit
