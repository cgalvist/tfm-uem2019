# Ejemplos de perfiles y versiones de kernel 


| Versión kernel | Arch   | API | Dispositivo  | Nombre                            | Perfil Volatility                  |
|:--------------:|:------:|:---:|:------------:|:---------------------------------:|:----------------------------------:|
| 3.10.0         | x86_64 | 21  | Goldfish     | goldfish_kernel_3.10.0_x86_64.zip | goldfish_profile_3.10.0_x86_64.zip |
