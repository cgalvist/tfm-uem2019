# TFM-UEM2019

Repositorio con scripts y documentación del Trabajo de Fin de Máster del Máster Universitario en Seguridad de las Tecnologías de la Información y las Comunicaciones de la Universidad Europea de Madrid (2019)

## Descripción

El script `main.sh` permite automatizar la creación de un entorno para analizar datos en RAM de dispositivos Android con Volatility.

El script ha sido probado con una máquina virtual con Android 5.1 (API 22) con procesadores x86 y x86_64. 
Recomendamos el uso de una máquina con procesador x86_64 para mayor facilidad de instalación.

## Requerimientos

El script se ha probado en Ubuntu 18.04. Las dependencias requeridas se pueden instalar con la siguiente orden:

```bash
sudo apt-get install dwarfdump pcregrep libpcre++-dev python-dev libelf-dev adb git libtool -y
```

Las dependencias requeridas para Volatility se pueden descargar con la siguiente orden:

```bash
pip install pycrypto && pip install distorm3 && pip install yara-python
```

## Archivos y carpetas:

* `Example_configs/`: Carpeta con ejemplos de configuraciones de kernel.
* `Examples/`: Carpeta con ejemplos de un kernel y un perfil de Volatility funcionales
* `Scripts/`: Carpeta con scripts y Makefiles de ejemplo
* `.user_config.sh`: Archivo con ajustes del usuario
* `.main.sh`: Script del programa principal
