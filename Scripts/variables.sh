# rutas
export MAIN_PATH=$PWD
export TC_PATH=$MAIN_PATH/Toolchain/$ARCH
export KSRC_PATH=$MAIN_PATH/Kernel/$KVER
export CC_PATH=$TC_PATH/bin
export LIME_SRC=$MAIN_PATH/LiME/src
export VOL_SRC=$MAIN_PATH/Volatility
export EMU_SRC=$HOME/Android/Sdk/emulator

# variables para los scripts
export CROSS_COMPILE="$CC_PATH/$SUBARCH$POSTFIX"
export KERNEL_OUT="bzImage"

export ADB=adb

export LIME_HOST_PORT="4444"
export LIME_DEVICE_PORT="4444"
export LIME_DUMP_NAME="ram.lime"
